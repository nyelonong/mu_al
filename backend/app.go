package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"

	"github.com/nyelonong/gommon/jsonapi"

	"database/sql"

	_ "github.com/go-sql-driver/mysql"
)

var (
	db *sql.DB
)

type User struct {
	ID        int64           `json:"id"`
	Email     string          `json:"email"`
	FullName  sql.NullString  `json:"fullname"`
	Address   sql.NullString  `json:"address"`
	Telephone sql.NullString  `json:"telephone"`
	Latitude  sql.NullFloat64 `json:"latitude"`
	Longitude sql.NullFloat64 `json:"longitude"`
}

type UserRaw struct {
	ID        int64   `json:"id"`
	Email     string  `json:"email"`
	FullName  string  `json:"fullname"`
	Address   string  `json:"address"`
	Telephone string  `json:"telephone"`
	Latitude  float64 `json:"latitude"`
	Longitude float64 `json:"longitude"`
}

func init() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	var err error
	db, err = sql.Open("mysql", "root:secret@tcp(127.0.0.1:3306)/accSchema")
	if err != nil {
		log.Fatalln(err)
		return
	}
}

func main() {
	mux := http.NewServeMux()
	mux.HandleFunc("/reg", regHandler)
	mux.HandleFunc("/get-profile", getProfileInfoHandler)
	mux.HandleFunc("/edit-profile", editProfileInfoHandler)
	mux.HandleFunc("/login", loginHandler)
	mux.HandleFunc("/login-google", loginGoogleHandler)
	mux.HandleFunc("/update-password", updatePasswordHandler)

	log.Println("Backend Listening...")
	log.Fatal(http.ListenAndServe(":8081", mux))
}

func regHandler(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()

	api := jsonapi.New(w, "")
	email := r.FormValue("email")
	pass := r.FormValue("pass")

	if email == "" || pass == "" {
		api.ErrorWriter(http.StatusBadRequest, "Empty body.")
		return
	}

	insert, err := db.Exec(
		"INSERT INTO profile (email, password) VALUES (?, ?)",
		email, pass,
	)
	if err != nil {
		log.Println(err)
		api.ErrorWriter(http.StatusInternalServerError, err.Error())
		return
	}

	uID, err := insert.LastInsertId()
	if err != nil {
		log.Println(err)
		api.ErrorWriter(http.StatusInternalServerError, err.Error())
		return
	}

	api.SuccessWriter(
		map[string]int64{
			"id": uID,
		},
	)
}

func loginHandler(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()

	api := jsonapi.New(w, "")
	email := r.FormValue("email")
	pass := r.FormValue("pass")

	if email == "" || pass == "" {
		api.ErrorWriter(http.StatusBadRequest, "Empty Body")
		return
	}

	var user User
	if err := db.QueryRow(
		"SELECT id, email, full_name, address, telephone, latitude, longitude FROM profile where email = ? and password = ?", email, pass,
	).Scan(&user.ID, &user.Email, &user.FullName, &user.Address, &user.Telephone, &user.Latitude, &user.Longitude); err != nil {
		log.Println(err)
		api.ErrorWriter(http.StatusInternalServerError, err.Error())
		return
	}

	api.SuccessWriter(user)
}

func loginGoogleHandler(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()

	api := jsonapi.New(w, "")
	email := r.FormValue("email")

	if email == "" {
		api.ErrorWriter(http.StatusBadRequest, "Empty Body")
		return
	}

	var user User
	if err := db.QueryRow(
		"SELECT id, email, full_name, address, telephone, latitude, longitude FROM profile where email = ? ", email,
	).Scan(&user.ID, &user.Email, &user.FullName, &user.Address, &user.Telephone, &user.Latitude, &user.Longitude); err != nil {
		log.Println(err)
		api.ErrorWriter(http.StatusInternalServerError, err.Error())
		return
	}

	api.SuccessWriter(user)
}

func getProfileInfoHandler(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()

	api := jsonapi.New(w, "")
	uID := r.FormValue("user_id")

	userID, err := strconv.ParseInt(uID, 10, 64)
	if err != nil {
		log.Println(err)
		api.ErrorWriter(http.StatusBadRequest, err.Error())
		return
	}

	var user User
	if err := db.QueryRow(
		"SELECT id, email, full_name, address, telephone, latitude, longitude FROM profile where id = ?", userID,
	).Scan(&user.ID, &user.Email, &user.FullName, &user.Address, &user.Telephone, &user.Latitude, &user.Longitude); err != nil {
		log.Println(err)
		api.ErrorWriter(http.StatusInternalServerError, err.Error())
		return
	}

	api.SuccessWriter(user)
}

func editProfileInfoHandler(w http.ResponseWriter, r *http.Request) {
	api := jsonapi.New(w, "")

	var user UserRaw

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		api.ErrorWriter(http.StatusBadRequest, err.Error())
		return
	}

	if err := json.Unmarshal([]byte(body), &user); err != nil {
		api.ErrorWriter(http.StatusBadRequest, err.Error())
		return
	}

	_, err = db.Exec(
		"UPDATE profile SET email = ?, full_name = ?, address = ?, telephone = ?, latitude = ?, longitude = ? WHERE id = ?",
		user.Email, user.FullName, user.Address, user.Telephone, user.Latitude, user.Longitude, user.ID,
	)
	if err != nil {
		log.Println(err)
		api.ErrorWriter(http.StatusInternalServerError, err.Error())
		return
	}

	var res User
	if err := db.QueryRow(
		"SELECT id, email, full_name, address, telephone, latitude, longitude FROM profile where id = ?", user.ID,
	).Scan(&res.ID, &res.Email, &res.FullName, &res.Address, &res.Telephone, &res.Latitude, &res.Longitude); err != nil {
		log.Println(err)
		api.ErrorWriter(http.StatusInternalServerError, err.Error())
		return
	}

	api.SuccessWriter(res)
}

func updatePasswordHandler(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	api := jsonapi.New(w, "")

	id, err := strconv.ParseInt(r.FormValue("id"), 10, 64)
	if err != nil {
		log.Println(err)
		api.ErrorWriter(http.StatusBadRequest, "Empty Body")
		return
	}

	pass := r.FormValue("pass")
	if pass == "" {
		api.ErrorWriter(http.StatusBadRequest, "Empty Body")
		return
	}

	_, err = db.Exec(
		"UPDATE profile SET password = ? WHERE id = ?",
		pass, id,
	)
	if err != nil {
		log.Println(err)
		api.ErrorWriter(http.StatusInternalServerError, err.Error())
		return
	}

	api.SuccessWriter(http.StatusText(http.StatusOK))
}
