# Backend Accounts

### How to Run
 - Setup mysql server
 ```sh
docker run -d --name mysql -p 3306:3306 -e MYSQL_ROOT_PASSWORD=secret -e MYSQL_DATABASE=accSchema mysql:5
 ```
 - Create table from `schema.sql`
 - Get all external lib
 ```sh
go get -u -v
 ```
 - Run the app
 ```sh
go run app.go
 ```