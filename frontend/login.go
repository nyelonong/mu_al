package main

import (
	"crypto/sha1"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"time"
)

func initLoginRouter(mux *http.ServeMux) {
	mux.HandleFunc("/login", loginHandler)
	mux.HandleFunc("/logout", logoutHandler)
}

func loginPageHandler(w http.ResponseWriter, r *http.Request) {
	t, _ := template.ParseFiles("template/login.html")
	t.Execute(w, nil)
}

func loginHandler(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()

	email := r.FormValue("email")
	pass := fmt.Sprintf("%x", sha1.Sum([]byte(r.FormValue("password"))))

	user, err := loginAPI(email, pass)
	if err != nil {
		log.Println(err)
		var view struct {
			Status string
		}

		view.Status = "User/email is not registered yet."

		t, _ := template.ParseFiles("template/login.html")
		t.Execute(w, view)
		return
	}

	cookie := http.Cookie{
		Name:    CookieKey,
		Value:   SetSession(user.ID),
		Expires: time.Now().Add(time.Hour),
		Path:    "/",
		Domain:  Domain,
	}
	http.SetCookie(w, &cookie)
	http.Redirect(w, r, "/info", http.StatusFound)
	return
}

func logoutHandler(w http.ResponseWriter, r *http.Request) {
	cookie, err := r.Cookie(CookieKey)
	if err == nil {
		if err := RemoveSession(cookie.Value); err == nil {
			http.SetCookie(w, &http.Cookie{
				Name:    CookieKey,
				Value:   cookie.Value,
				Expires: time.Now().Add(-24 * time.Hour),
			})

			http.Redirect(w, r, "/", http.StatusFound)
			return
		}
	}

	indexHandler(w, r)
	return
}
