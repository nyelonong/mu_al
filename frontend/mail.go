package main

import (
	"fmt"
	"log"
	"os"

	"github.com/sendgrid/sendgrid-go"
	"github.com/sendgrid/sendgrid-go/helpers/mail"
)

func SendResetPasswordMail(user User, otp int) error {
	from := mail.NewEmail("Accounts Test", "accounts@afrani.id")
	subject := "Forget Password Accounts"
	to := mail.NewEmail(user.FullName.String, user.Email)
	plainContent := fmt.Sprintf("Hi, %s!", user.FullName.String)
	htmlContent := fmt.Sprintf(`
		<strong>Please click button below to reset your password</strong>
		<a href='http://localhost:8080/edit-password?otp=%d&id=%d'>RESET</a>
	`, otp, user.ID)
	message := mail.NewSingleEmail(from, subject, to, plainContent, htmlContent)
	client := sendgrid.NewSendClient(os.Getenv("SENDGRID_API_KEY"))
	_, err := client.Send(message)
	if err != nil {
		log.Println(err)
	}

	return err
}
