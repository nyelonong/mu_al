package main

import (
	"log"
	"net/http"
	"os"
	"time"

	"github.com/patrickmn/go-cache"

	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
)

const (
	BackendURL string = "http://localhost:8081"
	Domain     string = "http://localhost:8080"
)

var googleOauthConfig = &oauth2.Config{
	RedirectURL:  "http://localhost:8080/callback",
	ClientID:     os.Getenv("GOOGLE_CLIENT_ID"),
	ClientSecret: os.Getenv("GOOGLE_CLIENT_SECRET"),
	Scopes: []string{
		"https://www.googleapis.com/auth/userinfo.profile",
		"https://www.googleapis.com/auth/userinfo.email"},
	Endpoint: google.Endpoint,
}

var MapBoxToken string = os.Getenv("MAPBOX_TOKEN")

var caching *cache.Cache

func init() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	caching = cache.New(time.Hour, time.Minute)
}

func main() {
	mux := http.NewServeMux()
	mux.HandleFunc("/", indexHandler)
	initGoogleRouter(mux)
	initLoginRouter(mux)
	initPasswordRouter(mux)
	initProfileRouter(mux)
	initRegisterRouter(mux)

	log.Println("Frontend Listening...")
	log.Fatal(http.ListenAndServe(":8080", mux))
}

func indexHandler(w http.ResponseWriter, r *http.Request) {
	cookie, err := r.Cookie(CookieKey)
	if err == nil {
		if CheckSession(cookie.Value) {
			profileHandler(w, r)
			return
		}
	}

	loginPageHandler(w, r)
}

func Middleware(next http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Cache-Control", "no-cache")
		w.Header().Set("Cache-Control", "no-store")
		next.ServeHTTP(w, r)
	})
}
