package main

import (
	"crypto/sha1"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	"github.com/dchest/uniuri"
	"golang.org/x/oauth2"
)

type GoogleUser struct {
	ID            string `json:"id"`
	Email         string `json:"email"`
	VerifiedEmail bool   `json:"verified_email"`
	Name          string `json:"name"`
	GivenName     string `json:"given_name"`
	FamilyName    string `json:"family_name"`
	Link          string `json:"link"`
	Picture       string `json:"picture"`
	Gender        string `json:"gender"`
	Locale        string `json:"locale"`
}

func initGoogleRouter(mux *http.ServeMux) {
	mux.HandleFunc("/callback", callbackHandler)
	mux.HandleFunc("/google", loginGoogleHandler)
}

func loginGoogleHandler(w http.ResponseWriter, r *http.Request) {
	oauthStateString := uniuri.New()
	url := googleOauthConfig.AuthCodeURL(oauthStateString)
	http.Redirect(w, r, url, http.StatusTemporaryRedirect)
}

func callbackHandler(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()

	code := r.FormValue("code")
	token, err := googleOauthConfig.Exchange(oauth2.NoContext, code)
	if err != nil {
		fmt.Fprintf(w, "Code exchange failed with error %s\n", err.Error())
		return
	}

	if !token.Valid() {
		fmt.Fprintln(w, "Retreived invalid token")
		return
	}

	response, err := http.Get("https://www.googleapis.com/oauth2/v2/userinfo?access_token=" + token.AccessToken)
	if err != nil {
		fmt.Fprintf(w, "Error getting user from token %s\n", err.Error())
		return
	}

	defer response.Body.Close()
	contents, err := ioutil.ReadAll(response.Body)

	var user *GoogleUser
	err = json.Unmarshal(contents, &user)
	if err != nil {
		log.Printf("Error unmarshaling Google user %s\n", err.Error())
		return
	}

	userRaw, err := checkMailAPI(user.Email)
	if err != nil {
		log.Println(err, "qq")
		autoGeneratePass := fmt.Sprintf("%x", sha1.Sum([]byte(time.Now().String())))
		id, err := registerAPI(user.Email, autoGeneratePass)
		if err != nil {
			log.Println(err)
			indexHandler(w, r)
			return
		}

		userRaw = &User{
			ID: id,
		}
	}

	cookie := http.Cookie{
		Name:    CookieKey,
		Value:   SetSession(userRaw.ID),
		Expires: time.Now().Add(time.Hour),
		Path:    "/",
		Domain:  Domain,
	}
	http.SetCookie(w, &cookie)

	http.Redirect(w, r, "/info", http.StatusFound)
	return
}
