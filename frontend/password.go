package main

import (
	"crypto/sha1"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"strconv"
)

func initPasswordRouter(mux *http.ServeMux) {
	mux.HandleFunc("/password", passwordHandler)
	mux.HandleFunc("/forget-password", forgetPasswordHandler)
	mux.HandleFunc("/edit-password", editPasswordHandler)
	mux.HandleFunc("/change-password", changePassword)
}

func passwordHandler(w http.ResponseWriter, r *http.Request) {
	t, _ := template.ParseFiles("template/password.html")
	t.Execute(w, nil)
}

func editPasswordHandler(w http.ResponseWriter, r *http.Request) {
	qv := r.URL.Query()

	var view struct {
		Status string
		UserID int64
		OTP    int
	}

	userID, err := strconv.ParseInt(qv.Get("id"), 10, 64)
	if err != nil {
		log.Println(err, qv.Get("id"))
		view.Status = "User is not valid."
		t, _ := template.ParseFiles("template/password.html")
		t.Execute(w, view)
		return
	}

	otp, err := strconv.Atoi(qv.Get("otp"))
	if err != nil {
		log.Println(err, qv.Get("otp"))
		view.Status = "OTP is not valid."
		t, _ := template.ParseFiles("template/password.html")
		t.Execute(w, view)
	}

	if !IsValidOTP(otp, userID) {
		view.Status = "OTP and user is not valid."
		t, _ := template.ParseFiles("template/password.html")
		t.Execute(w, view)
		return
	}

	view.UserID = userID
	view.Status = "Do not use your old password."
	view.OTP = otp
	t, _ := template.ParseFiles("template/edit-password.html")
	t.Execute(w, view)
}

func changePassword(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	var view struct {
		Status string
		UserID string
		OTP    string
	}
	view.UserID = r.FormValue("id")
	view.OTP = r.FormValue("otp")

	pass := fmt.Sprintf("%x", sha1.Sum([]byte(r.FormValue("password"))))
	userID, err := strconv.ParseInt(r.FormValue("id"), 10, 64)
	if err != nil {
		log.Println(err, r.FormValue("id"))
		view.Status = "User is not valid."
		t, _ := template.ParseFiles("template/edit-password.html")
		t.Execute(w, view)
		return
	}

	otp, err := strconv.Atoi(r.FormValue("otp"))
	if err != nil {
		log.Println(err, r.FormValue("otp"))
		view.Status = "OTP is not valid."
		t, _ := template.ParseFiles("template/password.html")
		t.Execute(w, view)
	}

	if !IsValidOTP(otp, userID) {
		view.Status = "OTP and user is not valid."
		t, _ := template.ParseFiles("template/password.html")
		t.Execute(w, view)
		return
	}

	if err := UpdatePassword(userID, pass); err != nil {
		view.Status = "Fail to update password. Please try again."
		t, _ := template.ParseFiles("template/password.html")
		t.Execute(w, view)
		return
	}

	RemoveOTP(userID)
	view.Status = "Password has been reset successfully."
	t, _ := template.ParseFiles("template/login.html")
	t.Execute(w, view)
	return
}

func forgetPasswordHandler(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	email := r.FormValue("email")

	var view struct {
		Status string
	}

	user, err := checkMailAPI(email)
	if err != nil {
		log.Println(err)
		view.Status = "User/email is not registered yet."
		t, _ := template.ParseFiles("template/password.html")
		t.Execute(w, view)
		return
	}

	if user.ID == 0 || user.Email == "" {
		view.Status = "User/email is not registered yet."
		t, _ := template.ParseFiles("template/password.html")
		t.Execute(w, view)
		return
	}

	SendResetPasswordMail(*user, SetOTP(user.ID))
	view.Status = "Email sent. Please check your inbox."
	t, _ := template.ParseFiles("template/password.html")
	t.Execute(w, view)
}
