package main

import (
	"encoding/json"
	"log"
	"net/url"
	"strconv"
	"time"

	"github.com/nyelonong/httpclient"
)

func loginAPI(email, pass string) (*User, error) {
	param := url.Values{}
	param.Add("email", email)
	param.Add("pass", pass)

	hc := httpclient.NewHTTPClient()
	hc.URL = BackendURL
	hc.Path = "/login"
	hc.Method = "POST"
	hc.Param = param
	hc.Timeout = 5 * time.Second

	body, err := hc.DoReq()
	if err != nil {
		log.Println(err)
		return nil, err
	}

	var resp User
	if err := json.Unmarshal(*body, &resp); err != nil {
		log.Println(err)
		return nil, err
	}

	return &resp, nil
}

func checkMailAPI(email string) (*User, error) {
	param := url.Values{}
	param.Add("email", email)

	hc := httpclient.NewHTTPClient()
	hc.URL = BackendURL
	hc.Path = "/login-google"
	hc.Method = "POST"
	hc.Param = param
	hc.Timeout = 5 * time.Second

	body, err := hc.DoReq()
	if err != nil {
		log.Println(err)
		return nil, err
	}

	var resp User
	if err := json.Unmarshal(*body, &resp); err != nil {
		log.Println(err)
		return nil, err
	}

	return &resp, nil
}

func getProfileAPI(userID int64) (*User, error) {
	param := url.Values{}
	param.Add("user_id", strconv.FormatInt(userID, 10))

	hc := httpclient.NewHTTPClient()
	hc.URL = BackendURL
	hc.Path = "/get-profile"
	hc.Method = "POST"
	hc.Param = param
	hc.Timeout = 5 * time.Second

	body, err := hc.DoReq()
	if err != nil {
		log.Println(err)
		return nil, err
	}

	var user User
	if err := json.Unmarshal(*body, &user); err != nil {
		log.Println(err)
		return nil, err
	}

	return &user, nil
}

func editProfileAPI(user UserRaw) (*User, error) {
	hc := httpclient.NewHTTPClient()
	hc.URL = BackendURL
	hc.Path = "/edit-profile"
	hc.Method = "POST"
	hc.JSON = user
	hc.Timeout = 5 * time.Second
	hc.IsJSON = true

	body, err := hc.DoReq()
	if err != nil {
		log.Println(err)
		return nil, err
	}

	var result User
	if err := json.Unmarshal(*body, &result); err != nil {
		log.Println(err)
		return nil, err
	}

	return &result, nil
}

func registerAPI(email, pass string) (int64, error) {
	param := url.Values{}
	param.Add("email", email)
	param.Add("pass", pass)

	hc := httpclient.NewHTTPClient()
	hc.URL = BackendURL
	hc.Path = "/reg"
	hc.Method = "POST"
	hc.Param = param
	hc.Timeout = 5 * time.Second

	body, err := hc.DoReq()
	if err != nil {
		log.Println(err)
		return 0, err
	}

	var resp map[string]int64
	if err := json.Unmarshal(*body, &resp); err != nil {
		log.Println(err)
		return 0, err
	}

	return resp["id"], nil
}

func UpdatePassword(id int64, pass string) error {
	param := url.Values{}
	param.Add("id", strconv.FormatInt(id, 10))
	param.Add("pass", pass)

	log.Println(param)

	hc := httpclient.NewHTTPClient()
	hc.URL = BackendURL
	hc.Path = "/update-password"
	hc.Method = "POST"
	hc.Param = param
	hc.Timeout = 5 * time.Second

	_, err := hc.DoReq()
	if err != nil {
		log.Println(err)
		return err
	}

	return nil
}
