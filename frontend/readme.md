# Frontend Accounts

## Depedencies
### Google Oauth 

This is used for login/register via google. 
Put this on your bash profile.
    
```sh
export GOOGLE_CLIENT_ID=YOURKEY
export GOOGLE_CLIENT_SECRET=YOURKEY
```

### MapBox

This is used for map and address. I use this instead of google maps because it's free. Should be not much different with google maps.

```sh
export MAPBOX_TOKEN=YOURKEY
```

### Sendgrid

This is used for send email. I use this because thay can provice free smtp for little use.

```sh
export SENDGRID_API_KEY=YOURKEY
```

## How to Run
    
```sh
go get -u -v
go install
frontend
```

## Demo
http://accounts.afrani.id