package main

import (
	"crypto/sha1"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"time"
)

func initRegisterRouter(mux *http.ServeMux) {
	mux.HandleFunc("/register", registerViewHandler)
	mux.HandleFunc("/reg", regHandler)
}

func registerViewHandler(w http.ResponseWriter, r *http.Request) {
	t, _ := template.ParseFiles("template/register.html")
	t.Execute(w, nil)
}

func regHandler(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()

	email := r.FormValue("email")
	pass := fmt.Sprintf("%x", sha1.Sum([]byte(r.FormValue("password"))))

	userRaw, err := checkMailAPI(email)
	if err == nil && userRaw.ID != 0 {
		var view struct {
			Status string
		}

		view.Status = "User/email is already registered."

		t, _ := template.ParseFiles("template/login.html")
		t.Execute(w, view)
	}

	id, err := registerAPI(email, pass)
	if err != nil {
		log.Println(err)
		indexHandler(w, r)
		return
	}

	cookie := http.Cookie{
		Name:    CookieKey,
		Value:   SetSession(id),
		Expires: time.Now().Add(time.Hour),
		Path:    "/",
		Domain:  Domain,
	}
	http.SetCookie(w, &cookie)

	profileHandler(w, r)
	return
}
