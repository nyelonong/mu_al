package main

import (
	"html/template"
	"log"
	"net/http"
	"strconv"
)

type View struct {
	Email     string
	FullName  string
	Address   string
	Telephone string
	SessionID string
}

func initProfileRouter(mux *http.ServeMux) {
	mux.HandleFunc("/info", Middleware(profileHandler))
	mux.HandleFunc("/profile", Middleware(editProfilePageHandler))
	mux.HandleFunc("/editprofile", editProfileHandler)
}

func editProfilePageHandler(w http.ResponseWriter, r *http.Request) {
	cookie, err := r.Cookie(CookieKey)
	if err != nil {
		log.Println(err)
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		return
	}

	if !CheckSession(cookie.Value) {
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		return
	}

	uID, err := GetUserID(cookie.Value)
	if err != nil {
		log.Println(err)
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		return
	}

	user, err := getProfileAPI(uID)
	if err != nil {
		log.Println(err)
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		return
	}

	var view struct {
		User
		MapBoxToken string
	}
	view.User = *user
	view.MapBoxToken = MapBoxToken

	t, _ := template.ParseFiles("template/edit-profile.html")
	t.Execute(w, view)
}

func profileHandler(w http.ResponseWriter, r *http.Request) {
	cookie, err := r.Cookie(CookieKey)
	if err != nil {
		log.Println(err)
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		return
	}

	if !CheckSession(cookie.Value) {
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		return
	}

	uID, err := GetUserID(cookie.Value)
	if err != nil {
		log.Println(err)
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		return
	}

	user, err := getProfileAPI(uID)
	if err != nil {
		log.Println(err)
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		return
	}

	temp := "template/edit-profile.html"
	if user.FullName.Valid {
		temp = "template/profile.html"
	}

	var view struct {
		User
		MapBoxToken string
	}
	view.User = *user
	view.MapBoxToken = MapBoxToken

	t, _ := template.ParseFiles(temp)
	t.Execute(w, view)
}

func editProfileHandler(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()

	var view struct {
		UserRaw
		Status string
	}

	email := r.FormValue("email")
	fullname := r.FormValue("fullname")
	address := r.FormValue("address")
	telephone := r.FormValue("telephone")

	cookie, err := r.Cookie(CookieKey)
	if err != nil {
		log.Println(err)
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		return
	}

	if !CheckSession(cookie.Value) {
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		return
	}

	uID, err := GetUserID(cookie.Value)
	if err != nil {
		log.Println(err)
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		return
	}

	user := UserRaw{
		ID:        uID,
		Email:     email,
		FullName:  fullname,
		Address:   address,
		Telephone: telephone,
	}

	latitude, err := strconv.ParseFloat(r.FormValue("latitude"), 64)
	if err != nil {
		log.Println(err)
		view.Status = "Failed. Latitude is not valid."
		t, _ := template.ParseFiles("template/edit-profile.html")
		t.Execute(w, view)
		return
	}

	longitude, err := strconv.ParseFloat(r.FormValue("longitude"), 64)
	if err != nil {
		log.Println(err)
		view.Status = "Failed. Longitude is not valid."
		t, _ := template.ParseFiles("template/edit-profile.html")
		t.Execute(w, view)
		return
	}

	user.Latitude = latitude
	user.Longitude = longitude
	view.UserRaw = user

	if email == "" || fullname == "" || address == "" || telephone == "" {
		view.Status = "Failed. Please fill all the field."
		t, _ := template.ParseFiles("template/edit-profile.html")
		t.Execute(w, view)
		return
	}

	usrEmail, err := checkMailAPI(email)
	if err != nil {
		log.Println(err)
		view.Status = "Failed check availability email."
		t, _ := template.ParseFiles("template/edit-profile.html")
		t.Execute(w, view)
		return
	}

	if usrEmail.ID != uID {
		view.Status = "Failed. Email already use by other user."
		t, _ := template.ParseFiles("template/edit-profile.html")
		t.Execute(w, view)
		return
	}

	_, err = editProfileAPI(user)
	if err != nil {
		log.Println(err)
		view.Status = "Failed."
		t, _ := template.ParseFiles("template/edit-profile.html")
		t.Execute(w, view)
	}

	http.Redirect(w, r, "/info", http.StatusFound)
	return
}
