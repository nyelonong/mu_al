package main

import (
	"database/sql"
	"encoding/base64"
	"errors"
	"fmt"
	"log"
	"math/rand"
	"strconv"
	"strings"

	"github.com/patrickmn/go-cache"
)

const (
	UserLoginKey  string = "accounts:login"
	CookieKey     string = "account_MU_AL"
	RandomString  string = "^$^utdutd^$#^$tydyt66"
	ForgetPassKey string = "accounts:otp:forget"
)

type User struct {
	ID        int64           `json:"id"`
	Email     string          `json:"email"`
	FullName  sql.NullString  `json:"fullname"`
	Address   sql.NullString  `json:"address"`
	Telephone sql.NullString  `json:"telephone"`
	Latitude  sql.NullFloat64 `json:"latitude"`
	Longitude sql.NullFloat64 `json:"longitude"`
}

type UserRaw struct {
	ID        int64   `json:"id"`
	Email     string  `json:"email"`
	FullName  string  `json:"fullname"`
	Address   string  `json:"address"`
	Telephone string  `json:"telephone"`
	Latitude  float64 `json:"latitude"`
	Longitude float64 `json:"longitude"`
}

func SetOTP(userID int64) int {
	key := fmt.Sprintf("%s:%d", ForgetPassKey, userID)
	otp := rand.Intn(9999)

	caching.Set(key, otp, cache.DefaultExpiration)
	return otp
}

func RemoveOTP(userID int64) {
	caching.Delete(fmt.Sprintf("%s:%d", ForgetPassKey, userID))
}

func IsValidOTP(code int, userID int64) bool {
	key := fmt.Sprintf("%s:%d", ForgetPassKey, userID)
	otp, found := caching.Get(key)
	return found && otp.(int) == code
}

func SetSession(uID int64) string {
	userSession := base64.StdEncoding.EncodeToString([]byte(fmt.Sprintf("%d:%s", uID, RandomString)))
	key := fmt.Sprintf("%s:%d", UserLoginKey, uID)

	caching.Set(key, userSession, cache.DefaultExpiration)
	return userSession
}

func GetUserID(us string) (int64, error) {
	sDec, err := base64.StdEncoding.DecodeString(us)
	if err != nil {
		log.Println(err)
		return 0, err
	}

	temp := strings.Split(string(sDec), ":")
	if len(temp) < 2 {
		return 0, errors.New("cookie not valid")
	}

	res, err := strconv.ParseInt(temp[0], 10, 64)
	if err != nil {
		log.Println(err)
		return 0, err
	}

	return res, nil
}

func CheckSession(us string) bool {
	sDec, err := base64.StdEncoding.DecodeString(us)
	if err != nil {
		log.Println(err)
		return false
	}

	temp := strings.Split(string(sDec), ":")
	if len(temp) < 2 {
		return false
	}

	key := fmt.Sprintf("%s:%s", UserLoginKey, temp[0])
	session, found := caching.Get(key)
	return found && session.(string) == us
}

func RemoveSession(us string) error {
	sDec, err := base64.StdEncoding.DecodeString(us)
	if err != nil {
		log.Println(err)
		return err
	}

	temp := strings.Split(string(sDec), ":")
	if len(temp) < 2 {
		return errors.New("cookie not valid")
	}

	key := fmt.Sprintf("%s:%s", UserLoginKey, temp[0])
	caching.Delete(key)
	return nil
}
